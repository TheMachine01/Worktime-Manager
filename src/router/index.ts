import { createRouter, createWebHistory } from '@ionic/vue-router';
// import { defineAsyncComponent } from 'vue'
import { RouteRecordRaw } from 'vue-router';
const HomePage = () => import('../views/HomePage.vue')
const TheSettings = () => import('../views/Settings/TheSettings.vue')
const SetDailyWorktime = () => import('../views/Settings/pages/SetDailyWorktime.vue')
const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    redirect: '/home',
    meta: { navigation: true }
  },
  {
    path: '/home',
    name: 'Home',
    meta: { navigation: true },
    component: HomePage
  },
  // settings routes
  {
    path: '/settings',
    name: 'Settings',
    meta: { navigation: true },
    component: TheSettings,
  },
  {
    path: '/settings/set-daily-worktime',
    name: 'Set Daily Worktime',
    meta: { navigation: false },
    component: SetDailyWorktime,
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
