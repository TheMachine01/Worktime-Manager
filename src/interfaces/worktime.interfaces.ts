

export interface WorktimeConf {
     day: {
          hours: number
          minutes: number
          created: string
          updated: string
     }
}
export interface WorktimeEntrie {
     from: string
     till: string,
     total: string,
     created: string
     updated: string
}

export interface WorktimeEntries {

     entries: Array<WorktimeEntrie>
     created: string
     updated: string
}

export interface WorktimeConf {
     day: {
          hours: number
          minutes: number
          created: string
          updated: string
     }
}
