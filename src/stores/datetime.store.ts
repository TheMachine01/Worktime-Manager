import { defineStore } from "pinia";

export interface State {
     chosenDate: string,


}

export const useDateTimeStore = defineStore({
     id: 'datetime',
     state: () => ({
          chosenDate: new Date().toISOString()
     } as State)

     ,
     getters: {
          getIsoString: (state) => state.chosenDate,
          getIsoStringOnlyDate: (state) => state.chosenDate.split('T')[0],

     },
     actions: {
          nrToStr(n: number) {
               if (n < 10) {
                    return `0${n}`
               }
               return n
          },
          getFormattedTime(payload: string) {
               const nrToStr = this.nrToStr
               console.log(`${nrToStr(new Date(payload).getHours())}-${nrToStr(new Date(payload).getMinutes())}`)
               return `${nrToStr(new Date(payload).getHours())}:${nrToStr(new Date(payload).getMinutes())}`
          },
          getTimeDiff(payload: string, payload2: string) {

               const p1: number = new Date(payload).getTime()
               const p2: number = new Date(payload2).getTime()

               const decimalResult = (((p2 - p1) / 1000) / (60 * 60)).toFixed(2)
               if (Number(decimalResult) < 0) { return false }
               const hours = parseFloat(decimalResult.toString().split('.')[0])
               const minutes = Math.round(((60 / 100) * Math.round(parseInt(decimalResult.toString().split('.')[1]))))

               const result = `${this.nrToStr(hours)}h${this.nrToStr(minutes)}`.replace('-', '')

               console.log(result)
               return result
          }

     }
})

