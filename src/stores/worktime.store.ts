import { defineStore } from 'pinia'
import { RemovableRef, useStorage } from '@vueuse/core'
import { useDateTimeStore } from './datetime.store'
import { WorktimeEntrie, WorktimeEntries, WorktimeConf } from '@/interfaces/worktime.interfaces'


const datetime = useDateTimeStore()



export interface State {
     worktimeData: RemovableRef<Map<string, WorktimeEntries>>,
     worktimeConf: RemovableRef<WorktimeConf>
}

export const useWorktimeStore = defineStore({
     id: 'worktime',
     state: () => ({
          worktimeData: useStorage("worktimeData",
               new Map<string, WorktimeEntries>(),

          ),
          worktimeConf: useStorage("worktimeConf", {
               day: {
                    'hours': 0,
                    'minutes': 0,
                    'created': new Date().toISOString(),
                    'updated': new Date().toISOString()

               }
          })

     } as State),
     getters: {
          getRegularWorkdayHours: (state) => {
               return state.worktimeConf.day.hours

          },
          getRegularWorkdayMinutes: (state) =>
               state.worktimeConf.day.minutes,


          getEntries: (state) => state.worktimeData.get(datetime.getIsoStringOnlyDate)?.entries ?? [],


          getWorkedTime: (state) => {
               const entries = state.worktimeData.get(datetime.getIsoStringOnlyDate)?.entries

               const totalAmounts: Array<number> = []

               const totalAmount = ''

               entries?.forEach((entrie: WorktimeEntrie) => {
                    console.log(entrie.total)
                    totalAmounts.push((parseFloat(entrie.total.split('h').join('.'))))
               })

               totalAmounts.forEach((amount) => { console.log(amount) })

          }
     }, actions: {
          addEntrie(entrie: WorktimeEntrie) {
               let newWorktimeMainEntrie: WorktimeEntries;
               if (this.worktimeData.get(datetime.getIsoStringOnlyDate)) {
                    newWorktimeMainEntrie = this.worktimeData.get(datetime.getIsoStringOnlyDate) as WorktimeEntries
                    newWorktimeMainEntrie?.entries.push(entrie)
               }
               else {

                    newWorktimeMainEntrie = { created: datetime.getIsoStringOnlyDate, updated: datetime.getIsoStringOnlyDate, entries: [entrie] }

               }
               this.worktimeData.set(datetime.getIsoStringOnlyDate, newWorktimeMainEntrie)

          },
          addEntries(entries: Array<WorktimeEntrie>) {
               for (let i = 0; i < entries.length; i++) {
                    let newWorktimeMainEntrie: WorktimeEntries;
                    if (this.worktimeData.get(datetime.getIsoStringOnlyDate)) {
                         newWorktimeMainEntrie = this.worktimeData.get(datetime.getIsoStringOnlyDate) as WorktimeEntries
                         newWorktimeMainEntrie?.entries.push(entries[i])
                    }
                    else {

                         newWorktimeMainEntrie = { created: datetime.getIsoStringOnlyDate, updated: datetime.getIsoStringOnlyDate, entries: [entries[i]] }

                    }
                    this.worktimeData.set(datetime.getIsoStringOnlyDate, newWorktimeMainEntrie)
               }
          }

     }
})

