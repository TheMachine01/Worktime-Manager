import { createApp, defineAsyncComponent } from 'vue'
import App from './App.vue'
import { createPinia } from 'pinia';
import router from './router';
import { IonicVue, IonIcon, IonPage } from '@ionic/vue';

const pinia = createPinia()
const DateChanger = defineAsyncComponent(() => import('./components/TheDateChanger.vue'))
const TheNavigation = defineAsyncComponent(() => import('./components/TheNavigation.vue'))
const VButton = defineAsyncComponent(() => import('./components/VButton.vue'))
const VDoubleButton = defineAsyncComponent(() => import('./components/VDoubleButton.vue'))
const VObserve = defineAsyncComponent(() => import('./components/VObserve.vue'))
const VInput = defineAsyncComponent(() => import('./components/VInput.vue'))
const VListWrapper = defineAsyncComponent(() => import('./components/List/VListWrapper.vue'))
const VListItem = defineAsyncComponent(() => import('./components/List/VListItem.vue'))
const VListSectionHeader = defineAsyncComponent(() => import('./components/List/VListSectionHeader.vue'))
const VHoverEvent = defineAsyncComponent(() => import('./components/VHoverEvent.vue'))
const VBadge = defineAsyncComponent(() => import('./components/VBadge.vue'))
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import SvgIcon from "@jamescoyle/vue-icon";
/* Core CSS required for Ionic components to work properly */
import '@ionic/vue/css/core.css';

// /* Basic CSS for apps built with Ionic */
import '@ionic/vue/css/normalize.css';
import '@ionic/vue/css/structure.css';
import '@ionic/vue/css/typography.css';

// /* Optional CSS utils that can be commented out */
// import '@ionic/vue/css/padding.css';
// import '@ionic/vue/css/float-elements.css';
// import '@ionic/vue/css/text-alignment.css';
// import '@ionic/vue/css/text-transformation.css';
// import '@ionic/vue/css/flex-utils.css';
// import '@ionic/vue/css/display.css';

/* Theme variables */
import './theme/variables.css';

const app = createApp(App)
  .use(IonicVue)
  .use(router)
  .use(pinia);

app.component('The-Date-Changer', DateChanger)
app.component('The-Navigation', TheNavigation)
app.component('Ion-Icon', IonIcon)
app.component('Ion-Page', IonPage)
app.component('Svg-Icon', SvgIcon)
app.component('V-Button', VButton)
app.component('V-Double-Button', VDoubleButton)
app.component('V-Observe', VObserve)
app.component('V-Input', VInput);
app.component('V-List-Wrapper', VListWrapper)
app.component('V-List-Item', VListItem)
app.component('V-List-Section-Header', VListSectionHeader)
app.component('V-Hover-Event', VHoverEvent)
app.component('V-Badge', VBadge)
router.isReady().then(() => {
  app.mount('#app');
});
